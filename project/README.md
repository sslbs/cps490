## CPS 490 Capstone I
### Department of Computer Science, University of Dayton
#### Instructor: Dr. Phu Phung 

### Team project
# Messenger Web Application 

## Final version (Sprint 3)
* Total points: 100 (20% of final grade)
* Sprint planned start date: 10/30/2020
* Sprint introduction: 11/03/2020 (Lecture 15)
* Sprint planned end date: 12/01/2020
* Instructions released on Isidore: 11/06/2020
* Presentations: 12/01/2020
* Submission deadline: 12/01/2020, 2:00 PM, accept until 12/03/2020 11:55 PM  

## Introduction 

In this final sprint, your team will continue revising your current prototype to add more functionalities. 
If your team did not complete any functionalities from previous sprints, your team needs to complete all by the end of this sprint.
As a final stage of this project, the outcome of this sprint is a complete working prototype and final documentation about the analysis, design, implementation, and evaluation.

## Preparation

Before starting this sprint, your team needs to review all the code to merge the "sprint2" branch to the master branch.
Similar to previous sprints, your team needs to create “`sprint3`” branch. 
To reduce the possible conflicts among members, it is highly recommended each member create a new branch within the new “`sprint3`” branch and continue to merge to “`sprint3`” branch when a task is completed.
As a reminder, never commit the code in the master branch as it will be difficult to resolve the conflicts.

## REQUIREMENTS AND GRADE DISTRIBUTION


1. Implementation: 70 points

    1.1. Functional Requirements

        1.1.1. (30 points) All functional requirements from previous sprints
        
        1.1.2. (10 points) Private and group chat messages are stored in the database
        
        1.1.3. (10 points) Users can read/load the history of private and group chat messages from the database
        
        1.1.4. (10 points) Two or more use cases of your team choice
    
    1.2. Non-functional requirements
    
        1.2.1. (5 points) The system must be secure and defend against simple web attacks: (i) Data interacted with the database must be validated;  (ii) All data must be sanitized on the client-side before displaying.
        1.2.3. (5 points) Passwords must be hashed in the database

    1.3. Repository 
    
        1.3.1. (2.5 points) Clean organization
        
        1.3.2. (2.5 points) No temporary and should-be-ignored files/folders such as node_modules 
        
        1.3.3. (2.5 points) Pushing on the branch, create a pull request, and merge to the master branch when the sprint is completed

    1.4 Deployment (5 points) The application is successfully deployed on the cloud 

2. Documentation: 20 points  

    2.1. Requirements:
    
        2.1.1. (3 points) Format: Use the provided README.md template from Sprint 0, with the following information: the course and instructor, the project and sprint information, your team members, and IDs. The project description should include Introduction, Analysis, Design, Implementation, and Evaluation and Progress Report. The progress report should mention **how many hours each team member contributed, explanation of roles for each team member** in this sprint. You need to provide **the link to the latest commit** for the sprint in this section.
        
        2.1.2. (1 point) Source code in a separate PDF file. 

    2.2. Analysis
    
        2.2.1. (1 point) Requirements, General User Case Diagram
        
        2.2.2. (5 points) Each use case: User Story, Brief Use Case Description, and either Fully Developed Use Case Description or System Sequence Diagram or Activity Diagram  

    2.3. Design
 
        2.3.1. (2 points) The general architecture of the system
        
        2.3.2. (3 points) Sequence (Interaction) diagrams for implemented use cases.
        
    2.4. Implementation
       
        2.4.1. (2.5 points) Code snippets with the explanation of each use case implemented  

    2.5. Evaluation

        2.5.1. (2.5 points) Screenshots of working use cases with a caption


3. Presentation: 10 points
    
    3.1. (5 points) Slides: Proper slides preparation (In Google Slides, send the link to the instructor before the presentation time)
    
    3.2. (5 points) Communication skills: Appropriate presentation 


## SUBMISSION

Submit the following files in Isidore by the deadline. One submission per team.

1. `project-final-team#-report.pdf` (# is your team number) converted from your README.md file. 

2. Source code in `project-final-team#-source-code.pdf`. Alternatively, your team can submit separate source files in .txt

3. Presentation slides in `project-final-team#-slides.pdf`

